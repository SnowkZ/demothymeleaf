package ca.qc.colval.demothymeleaf.repositories.interfaces;

import ca.qc.colval.demothymeleaf.models.entities.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IStoreRepository extends JpaRepository<Store, Long> {
}
