package ca.qc.colval.demothymeleaf.repositories.interfaces;

import ca.qc.colval.demothymeleaf.models.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAddressRepository extends JpaRepository<Address, Long> {
}
