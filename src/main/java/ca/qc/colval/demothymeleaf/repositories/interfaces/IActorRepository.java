package ca.qc.colval.demothymeleaf.repositories.interfaces;

import ca.qc.colval.demothymeleaf.models.entities.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface IActorRepository extends JpaRepository<Actor, Long> {

 List<Actor> findByFirstName(String firstName);
 List<Actor> findByLastName(String lastName);
 List<Actor> findByLastUpdate(Date lastUpdate);
 Long countAll();
 List<Actor> findAllActorIdAsc();

}