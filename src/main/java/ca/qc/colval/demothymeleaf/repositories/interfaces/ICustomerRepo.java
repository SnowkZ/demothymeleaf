package ca.qc.colval.demothymeleaf.repositories.interfaces;

import ca.qc.colval.demothymeleaf.models.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ICustomerRepo extends JpaRepository<Customer, Long> {
    List<Customer> findAllCustomerSortedByLastName();
    List<Customer> findByFirstNameSubStr(String subStr);
    List<Customer> findAllCustomerIdDesc();
}
