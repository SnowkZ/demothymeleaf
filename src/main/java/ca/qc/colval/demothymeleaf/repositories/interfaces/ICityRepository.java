package ca.qc.colval.demothymeleaf.repositories.interfaces;

import ca.qc.colval.demothymeleaf.models.entities.City;
import ca.qc.colval.demothymeleaf.models.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICityRepository extends JpaRepository <City, Long> {
}
