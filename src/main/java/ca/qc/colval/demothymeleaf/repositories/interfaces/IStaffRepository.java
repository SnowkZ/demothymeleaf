package ca.qc.colval.demothymeleaf.repositories.interfaces;

import ca.qc.colval.demothymeleaf.models.entities.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IStaffRepository extends JpaRepository<Staff, Long> {
}
