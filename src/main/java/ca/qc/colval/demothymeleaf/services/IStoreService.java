package ca.qc.colval.demothymeleaf.services;

import ca.qc.colval.demothymeleaf.models.entities.City;
import ca.qc.colval.demothymeleaf.models.entities.Store;

import java.util.List;
import java.util.Optional;

public interface IStoreService {
    Optional<Store> readOne(Long storeId);

    List<Store> readAll();

    void delete(Long storeId);

    Long countAllStore();
}
