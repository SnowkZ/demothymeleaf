package ca.qc.colval.demothymeleaf.services;

import ca.qc.colval.demothymeleaf.models.entities.Staff;

import java.util.List;
import java.util.Optional;

public interface IStaffService {
    Optional<Staff> readOne(Long staffId);

    List<Staff> readAll();

    void delete(Long staffId);

    List<Staff> getAllTenRandomStaff();

}
