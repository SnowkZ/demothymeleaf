package ca.qc.colval.demothymeleaf.services;

import ca.qc.colval.demothymeleaf.models.entities.Country;

import java.util.List;
import java.util.Optional;

public interface ICountryService {
    Optional<Country> readOne(Long countryId);

    List<Country> readAll();

    void delete(Long countryId);

    Long countAllCountry();

}
