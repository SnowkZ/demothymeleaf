package ca.qc.colval.demothymeleaf.controllers;

import ca.qc.colval.demothymeleaf.services.implementation.ActorService;
import ca.qc.colval.demothymeleaf.services.implementation.CustomerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/actor")
public class actorController {
    ActorService actorService;

    public actorController(ActorService actorService) {
        this.actorService = actorService;
    }

    @GetMapping("/all")
    public String customer(Model model){
        model.addAttribute("allActor", actorService.findAllActorIdAscAndLimitTen());
        return "actor/actor";
    }
}
