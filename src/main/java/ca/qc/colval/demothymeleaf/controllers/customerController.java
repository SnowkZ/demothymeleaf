package ca.qc.colval.demothymeleaf.controllers;

import ca.qc.colval.demothymeleaf.controllers.viewmodels.SearchNameViewModel;
import ca.qc.colval.demothymeleaf.models.entities.Customer;
import ca.qc.colval.demothymeleaf.services.implementation.CustomerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/customer")
public class customerController {
    CustomerService customerService;


    public customerController(CustomerService customerService) {
        this.customerService = customerService;
    }

        @GetMapping("/all")
        public String customer(Model model, SearchNameViewModel searchNameViewModel){
        model.addAttribute("searchNames", new SearchNameViewModel());
        model.addAttribute("allCustomer", customerService.findAllCustomerIdDescAndLimitTen());
        model.addAttribute("customerCount", customerService.readAll().size());

        return "customer/customer";
    }


    @PostMapping("/do_search_name")
    public String searchCustomerByName(Model model, SearchNameViewModel searchNameViewModel){
        System.out.println(searchNameViewModel.getFirstname());
        String firstNameSubStr = searchNameViewModel.getFirstname();
        List<Customer> customersWithName = customerService.getAllCustomerWithFirstNameSubStr(firstNameSubStr);
        int nbCustomers = customersWithName.size();
        model.addAttribute("allCustomer", customersWithName);
        model.addAttribute("customerCount", nbCustomers);
        model.addAttribute("searchNames", new SearchNameViewModel(firstNameSubStr));
        return "customer/customer";
    }

    @GetMapping("/{id}")
    public String getCustomer(Model model, @PathVariable Long id){

        Optional<Customer> customer=customerService.readOne(id);

        model.addAttribute("customerId", id);
        model.addAttribute("customer",customer.get());

        return "customer/detail";

    }

}
