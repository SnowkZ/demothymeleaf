package ca.qc.colval.demothymeleaf.controllers;

import ca.qc.colval.demothymeleaf.services.implementation.CustomerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class indexController {
    CustomerService customerService;

    public indexController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/") // GetMapping root = ressources/templates
    public String index(Model model) {
        //Compter les customers
        model.addAttribute("nbCustomer", customerService.countAllCustomer());

        return "index/index"; // folder/file
    }



}
